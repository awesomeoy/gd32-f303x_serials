#ifndef APP_H
#define APP_H
	#include "rtthread.h"
	#include "gd32f30x.h"
	#include "serial.h"
	#include "led.h"
	
	extern Led led_run_obj;
	extern struct boardinfo board_info;
		
	void app_init(void);
	void app(void);
#endif

