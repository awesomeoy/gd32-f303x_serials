#ifndef LED_H
#define LED_H
	#include "gd32f30x.h"
	
	#define LED_ON  RESET
	#define LED_OFF SET
	
	typedef struct
	{
		uint32_t gpio_periph;
		uint32_t pin;
		bit_status status;
	}Led;
	
	void led_init(Led* led, uint32_t gpio_periph, uint32_t pin, bit_status status);
	void led_toggle(Led* led);
	void led_on(Led* led);
	void led_off(Led* led);
	
#endif
