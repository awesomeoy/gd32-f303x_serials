#include "led.h"

void led_init(Led* led, uint32_t gpio_periph, uint32_t pin, bit_status status)
{
	led->gpio_periph = gpio_periph;
	led->pin = pin;
	led->status = status;
	gpio_init(gpio_periph, GPIO_MODE_OUT_PP, GPIO_OSPEED_10MHZ, pin);
	gpio_bit_write(gpio_periph, pin, status);
}

void led_toggle(Led* led)
{
	gpio_bit_write(led->gpio_periph, led->pin, led->status==RESET?SET:RESET);
	led->status = led->status==RESET?SET:RESET;
}

void led_on(Led* led)
{
	gpio_bit_write(led->gpio_periph, led->pin, LED_ON);
	led->status = LED_ON;
}

void led_off(Led* led)
{
	gpio_bit_write(led->gpio_periph, led->pin, LED_OFF);
	led->status = LED_OFF;
}
