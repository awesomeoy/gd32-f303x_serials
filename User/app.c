
#include "app.h"
#include "string.h"

Led led_run_obj;

static void board_init(void)
{		
	rcu_periph_clock_enable(RCU_GPIOB);
	led_init(&led_run_obj, GPIOB, GPIO_PIN_12, LED_OFF);
}

//LED闪烁任务
static void led_task(void* param)
{
	while(1)
	{	
		serial_write(SERIAL_ID_1, (uint8_t*)"Led Task Running\r\n", strlen("Led Task Running\r\n"));
		serial_write(SERIAL_ID_2, (uint8_t*)"Led Task Running\r\n", strlen("Led Task Running\r\n"));
		serial_write(SERIAL_ID_3, (uint8_t*)"Led Task Running\r\n", strlen("Led Task Running\r\n"));
		rt_thread_mdelay(1000);
		led_toggle(&led_run_obj);
	}
}

//串口数据处理任务
static void serial_process_task(void* param)
{
	uint8_t rec_data[256];
	uint32_t len = 0;
	while(1)
	{
		uint8_t i = 0;
		for( ; i<USART_MAX_NUM; ++i )
		{
			if( serials[i]!=(void*)0 )
			{
				SERIAL_ID id = serials[i]->id;
				switch(id)
				{
					case SERIAL_ID_1:
						/* 回环测试，将串口接收到的数据发送出去 */
						len = serial_read(SERIAL_ID_1, rec_data, 256);
						if( len>0 )
						{
							serial_write(SERIAL_ID_1, (uint8_t*)"R: ", strlen("R: "));
							serial_write(SERIAL_ID_1, rec_data, len);
						}
						break;
					case SERIAL_ID_2:
						len = serial_read(SERIAL_ID_2, rec_data, 256);
						if( len>0 )
						{
							serial_write(SERIAL_ID_2, (uint8_t*)"R: ", strlen("R: "));
							serial_write(SERIAL_ID_2, rec_data, len);
						}
						break;
					case SERIAL_ID_3:
						len = serial_read(SERIAL_ID_3, rec_data, 256);
						if( len>0 )
						{
							serial_write(SERIAL_ID_3, (uint8_t*)"R: ", strlen("R: "));
							serial_write(SERIAL_ID_3, rec_data, len);
						}
						break;
					case SERIAL_ID_USB:
						break;
					default:
						break;
				}	
				
				open_usart_send(serials[i]);			
			}
		}
		rt_thread_mdelay(5);
	}
}

void app_init(void)
{
	rt_thread_t led_thread;
	rt_thread_t serial_thread;
	serial_init();
	
	led_thread = rt_thread_create("LED",
																led_task,
																(void*)0,
																512,
																RT_THREAD_PRIORITY_MAX-1,
																10);
	serial_thread = rt_thread_create("SERIAL",
																	 serial_process_task,
																	 (void*)0,
																	 2048,
																	 RT_THREAD_PRIORITY_MAX-3,
																	 10);
	rt_thread_startup(led_thread);
	rt_thread_startup(serial_thread);
}

//主运行APP程序
void app(void)
{
	board_init();
	app_init();
}
