# GD32F303x_serials

#### 介绍
**基于RTThread+GD32F303x的串口框架，基于本人STM32串口框架进行的移植，地址:https://gitee.com/awesomeoy/STM32F103_Serials**
**采用DMA+空闲中断方式接收串口数据，采用DMA方式发送串口数据**

#### 开发说明

**集成开发软件IDE: Keil5**

**用户代码位于文件app.c中**

**框架核心代码位于文件serial.c中**

#### 调用说明

**串口初始化 serial_init()**

**线程中调用serial_write()函数往串口发送缓冲区中写入数据**

**线程中调用serial_read()函数读取串口接收缓冲区数据**

**app.c文件中serial_process_task()线程中定时调用open_usart_send()函数发送串口发送缓冲区中数据**

**serial.h文件中USART1_ENABLE，USART2_ENABLE，USART3_ENABLE宏定义使能串口**



